#!/bin/bash
current=''
skip=()
ask_continue=0
first=1
reboot=0

read -r -d '' help << EOM
Usage: ./script.sh [--skip XX .. XX] [--pause] [--reboot]

This is nano's mint setup script.

This script will do some usefull configurations. Check each script in the scripts folder to see what each does.

You can also create your own scripts, just save them with an .sh extension and the name MUST start with a two digit priority number (scripts with lower priority number are executed first).

  --skip XX .. XX   Skip all scripts in XX priorities listed.
  --pause           Wait for a key press at the end of each script.
  --reboot          Reboot at the end of excecution.]

EOM


for var in "$@"
do
    if [[ $var == '--help' ]] || [[ $var == '-h' ]]; then
        echo "$help"
        exit 0
    elif [[ $var == \-\-* ]]; then
        current=$var
    else
        if [[ $current == '--skip' ]]; then
            skip+=($var)
        elif [[ $current == '--pause' ]] || [[ $current == '--reboot' ]]; then
            echo "ERROR: $current command does not accept arguments."
            exit 1
        else
            echo "ERROR: $current command not recognized"
            exit 1
        fi
    fi

    if [[ $current == '--pause' ]]; then
        ask_continue=1
    fi
    if [[ $current == '--reboot' ]]; then
        reboot=1
    fi
done

for line in `find scripts/ | sort`
do
    if [[ $line == *.sh ]]; then
        priority=${line:8:2}
        to_skip=$(echo ${skip[@]} | grep -o "$priority" | wc -w)
        if [[ $to_skip == 0 ]]; then
            if [[ $ask_continue == 1 ]] && [[ $first == 0 ]]; then
                echo "Press key to continue"
                read -n 1 -s
            fi
            first=0
            echo
            echo "Running $line"
            echo
            /bin/bash $line
        else
            echo
            echo "Skipping $line"
            echo
        fi
    fi
done

if [[ $reboot == 1 ]]; then
    echo
    echo "Rebooting system"
    echo
    sudo reboot
fi
