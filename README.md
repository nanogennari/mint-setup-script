# Mint Setup script

![Release](https://img.shields.io/badge/dynamic/json?color=3776AB&label=Release&query=%24..name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Fnanogennari%252Fmint-setup-script%2Frepository%2Ftags&style=flat-square)

My personal script to setup new Linux Mint installations.
