#!/bin/bash
#
# This is out of date!!!!
#
if [ -z ${DOMAIN+x} ]; then DOMAIN="server.eurydyka.space"; fi
if [ -z ${DOCKER_DATA_FOLDER+x} ]; then DOCKER_DATA_FOLDER="/root/docker"; fi
if [ -z ${WEB_ROOT+x} ]; then WEB_ROOT="/var/www"; fi
mkdir $DOCKER_DATA_FOLDER

echo
echo "Update system"
apt update
apt upgrade -y
apt install curl -y

echo
echo "Install docker"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository   "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt install docker-ce -y

echo
echo "Install acme.sh"
apt install socat -y
curl https://get.acme.sh | sh

echo
echo "Issue certificates"
mkdir $DOCKER_DATA_FOLDER/certs
mkdir $DOCKER_DATA_FOLDER/certs/$DOMAIN
~/.acme.sh/acme.sh --issue --standalone -d $DOMAIN
~/.acme.sh/acme.sh --install-cert -d $DOMAIN \
--fullchain-file $DOCKER_DATA_FOLDER/certs/$DOMAIN/cert.crt \
--key-file $DOCKER_DATA_FOLDER/certs/$DOMAIN/cert.key \
--ca-file $DOCKER_DATA_FOLDER/certs/$DOMAIN/ca.pem \
--reloadcmd "docker restart nginx"

echo
echo "Launching portainer with SSL enabled"
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v portainer_data:/data \
-v $DOCKER_DATA_FOLDER/certs/$DOMAIN:/certs \
portainer/portainer-ce \
--ssl --sslcert /certs/cert.crt --sslkey /certs/cert.key

echo
echo "Launching nginx with SSL"
mkdir $DOCKER_DATA_FOLDER/nginx-confs
curl https://ssl-config.mozilla.org/ffdhe2048.txt > $DOCKER_DATA_FOLDER/nginx_dhparam.txt
echo "server {
    listen 80 default_server;
    server_name _;
    return 301 https://$DOMAIN\$request_uri;
}
server {
    listen 80;
    server_name portainer.$DOMAIN;
    return 301 https://$DOMAIN:9000;
}
server {
    listen 443 ssl http2;
    server_name $DOMAIN;

    ssl_certificate /certs/$DOMAIN/cert.crt;
    ssl_certificate_key /certs/$DOMAIN/cert.key;

    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /dhparam.txt
    ssl_dhparam /dhparam.txt;

    # intermediate configuration
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security \"max-age=63072000\" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /certs/$DOMAIN/ca.pem;

    root /www;
}" > $DOCKER_DATA_FOLDER/nginx-confs/default.conf
docker run -d -p 80:80 -p 443:443 --name=nginx --restart=always \
-v $DOCKER_DATA_FOLDER/nginx-confs:/etc/nginx/conf.d \
-v $DOCKER_DATA_FOLDER/certs:/certs \
-v $DOCKER_DATA_FOLDER/nginx_dhparam.txt:/dhparam.txt \
-v $WEB_ROOT:/www \
nginx:latest

echo
echo "Updating acme.sh challenge mode"
~/.acme.sh/acme.sh --issue -d $DOMAIN -w $WEB_ROOT

echo
echo "Setting default index and portainer redirect"
echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\"/>
        <title>$DOMAIN server</title>
    </head>
    <body>
        <h1>This connection shoud be secured.</h1>
        <p>Portainer sould be here:
          <a href=\"https://$DOMAIN:9000\">https://$DOMAIN:9000</a> or <a href=https://$DOMAIN/portainer>https://$DOMAIN/portainer</a></p>
        <p>Nginx shoud be here: <a href=\"https://$DOMAIN\">https://$DOMAIN</a></p>
    </body>
</html>" > $WEB_ROOT/index.html
mkdir $WEB_ROOT/portainer
echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\"/>
        <title>Portainer redirect</title>
        <meta http-equiv=\"refresh\" content=\"1; URL='https://$DOMAIN:9000'\"/>
    </head>
    <body>
        <h1>Redirecting to portainer!</h1>
    </body>
</html>" > $WEB_ROOT/portainer/index.html

echo "Configuration finished"
echo
echo "IMPORTANT: Configure portainer user NOW: https://$DOMAIN/portainer, when asked select Docker enviroment."

server {
    listen 80;
    server_name calango.club;
    return 301 https://$host$request_uri;
}
server {
    listen 443 ssl http2;
    server_name calango.club;

    ssl_certificate /certs/calango.club/cert.crt;
    ssl_certificate_key /certs/calango.club/cert.key;

    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /dhparam.txt
    ssl_dhparam /dhparam.txt;

    # intermediate configuration
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /certs/calango.club/ca.pem;

    root /www/calango.club;
}
