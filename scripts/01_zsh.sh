#!/bin/bash
echo
echo "Instalando zsh"
echo
sudo apt-get install -y zsh git
if [ -f /usr/bin/zsh ]
then
  echo
  echo "Selecionando zsh como shell padrão"
  echo
  sudo chsh -s /usr/bin/zsh $USER
else
  echo
  echo "/usr/bin/zsh não encontrado!"
  echo "Não foi possível selecionar zsh como shell padrão"
  echo
fi
echo "Instalando Oh-my-zsh"
echo
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
