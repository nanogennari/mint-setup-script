if [ -x "$(command -v conda)" ]
then
  echo
  echo "Anaconda already installed"
  echo
else
  anacondaLink="https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh"
  echo
  echo "Installing Anaconda"
  echo
  wget $anacondaLink -O /tmp/anaconda-installer.sh
  chmod a+x /tmp/anaconda-installer.sh
  /tmp/anaconda-installer.sh
  rm -f /tmp/anaconda-installer.sh
fi