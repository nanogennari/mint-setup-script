#!/bin/bash
echo
echo "Installing i3"
echo
sudo apt-get install -y i3 i3blocks git
echo "Clonning i3-cinnamon's repository"
echo
git clone https://github.com/jthomaschewski/i3-cinnamon.git
echo
echo "Installing i3-cinnamon"
echo
cd i3-cinnamon
sudo make install
cd ..
rm -Rf i3-cinnamon