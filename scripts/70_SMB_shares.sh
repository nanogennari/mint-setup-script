#!/bin/bash
echo
echo "Configuring smb in shares_smb.txt"  #Um compartilhamento por linha: servidor:/caminho/do/compartilhamento /pasta/para/montar
echo
if [[ -z "${SMB_OPTIONS}" ]]
then
  echo '$SMB_OPTIONS is not set skipping smb shares setup'
  echo
else
  filelines=`cat shares_smb.txt`
  first=1
  for line in $filelines ; do
    if [ $first -eq 1 ]
    then
      server=$line
      first=0
    else
      echo
      echo "Adding share: $server $line"
      echo
      echo "$server $line cifs $SMB_OPTIONS 0 0 " | sudo tee -a /etc/fstab
      first=1
    fi
  done
fi
