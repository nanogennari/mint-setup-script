#!/bin/bash
echo
echo "Updating sources"
echo
sudo apt-get update
echo
echo "Installing packages in packs.txt"
echo
filelines=`cat packs.txt`
while read -r line
do
    if [[ $line != \#* ]] && [[ $line != '' ]] ; then
        echo
        echo "Installing package: $line"
        echo
        sudo apt-get install --install-recommends -y $line
    fi
done < <(tail -n +1 packs.txt)