#!/bin/bash
echo
echo "Configuring nfs in shares_nfs.txt"  #Um compartilhamento por linha: servidor:/caminho/do/compartilhamento /pasta/para/montar
echo
filelines=`cat shares_nfs.txt`
first=1
for line in $filelines ; do
  if [ $first -eq 1 ]
  then
    server=$line
    first=0
  else
    echo
    echo "Adding share: $server $line"
    echo
    echo "$server $line nfs defaults 0 0 " | sudo tee -a /etc/fstab
    first=1
  fi
done