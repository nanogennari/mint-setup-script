#!/bin/bash
echo
echo "Installing vim"
echo
sudo apt-get install -y vim
if [ -f /usr/bin/vim.basic ]
then
  echo
  echo "Selecting /usr/bin/vim.basic as the default editor"
  echo
  sudo update-alternatives --set editor /usr/bin/vim.basic
else
  echo
  echo "/usr/bin/vim.basic not found!"
  echo "Please manually select vim as the default editor"
  echo
  sudo update-alternatives --config editor
fi