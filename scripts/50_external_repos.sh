#!/bin/bash
. /etc/os-release
MINT_CODENAME=`lsb_release -c | tr "\t" "\n" | tail -n 1`
#arch=`dpkg --print-architecture`

echo
echo "Installing external repositories"
echo


while IFS=";" read -r name keyURL arch repoURL packages
do
    repoURL=${repoURL//UBUNTU_CODENAME/"$UBUNTU_CODENAME"}
    repoURL=${repoURL//MINT_CODENAME/"$MINT_CODENAME"}
    echo
    echo "Installing $name's repository"
    echo
    keyPath="/usr/share/keyrings/$name.gpg"
    if [[ $keyURL == keyserver* ]]; then
        read -r trash keyserver key <<< $keyURL
        gpg --recv-keys --keyserver $keyserver $key
        gpg --export $key | sudo dd of=$keyPath
    else
        wget -qO- $keyURL | gpg --dearmor | sudo dd of=$keyPath
    fi
    echo "GPG key installed to $keyPath"

    repoPath="/etc/apt/sources.list.d/$name.list"
    if [[ $arch != '' ]]; then
        echo "deb [signed-by=$keyPath arch=$arch] $repoURL" | sudo dd of=$repoPath
    else
        echo "deb [signed-by=$keyPath] $repoURL" | sudo dd of=$repoPath
    fi
    echo "Repository listed on $repoPath"
    echo

    echo "Installing $name's packages: $packages"
    echo
    sudo apt update
    sudo apt-get install --install-recommends -y $packages
done < <(tail -n +2 external_repos.csv)
