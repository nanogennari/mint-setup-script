#!/bin/bash
echo
echo "Installing PPA repositories"
echo

while IFS=";" read -r name ppa packages
do
    echo
    echo "Installing $names's repository: $ppa"
    echo
    sudo add-apt-repository -y $ppa

    echo
    echo "Installing $name's packages: $packages"
    echo
    sudo apt update
    sudo apt-get install --install-recommends -y $packages
done < <(tail -n +2 ppa_repos.csv)

