user=`whoami`
count=`sudo cat /etc/sudoers | grep -c $user`
if [ "$count" -gt 0 ]
then
  echo
  echo "Sudo already configured"
  echo
else
  echo
  echo "Allowing no password sudo for user $user"
  echo
  sudo chmod u+w /etc/sudoers
  sudo su -c 'echo "nano ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers'
  sudo chmod u-w /etc/sudoers
fi